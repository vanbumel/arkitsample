//
//  SceneFileFormatEnum.h
//  SampleARKit
//
//  Created by OnSight on 5/14/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#ifndef SceneFileFormatEnum_h
#define SceneFileFormatEnum_h

/**
 @author Vladyslav Bedro
 
 Enumerations with file formats of scene models (".obj", ".dae", ".scn", etc.)
 */
typedef NS_ENUM(NSUInteger, SceneFileFormat)
{
    DaeFileFormat,
    ScnFileFormat,
    ObjFileFormat
};

#endif /* SceneFileFormatEnum_h */
