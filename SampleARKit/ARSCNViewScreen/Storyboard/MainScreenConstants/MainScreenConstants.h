//
//  MainScreenConstants.h
//  SampleARKit
//
//  Created by OnSight on 5/14/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#ifndef MainScreenConstants_h
#define MainScreenConstants_h

static NSString* kMainScreenCell           = @"MainScreenCell";
static NSString* kMainScreenCellIdentifier = @"MainScreenCellID";

#endif /* MainScreenConstants_h */
