//
//  TableModel.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/9/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TableDataDisplayManagerInput.h"

NS_ASSUME_NONNULL_BEGIN

@interface TableModel : NSObject <TableDataDisplayManagerInput>

@end

NS_ASSUME_NONNULL_END
