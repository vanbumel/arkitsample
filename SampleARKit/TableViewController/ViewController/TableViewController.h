//
//  TableViewController.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/9/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

//protocols
#import "TableDataDisplayManagerOutput.h"

NS_ASSUME_NONNULL_BEGIN

@interface TableViewController : UIViewController <TableDataDisplayManagerOutput>

@end

NS_ASSUME_NONNULL_END
