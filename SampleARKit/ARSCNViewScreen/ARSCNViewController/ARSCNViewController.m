//
//  ARSCNViewController.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/7/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "ARSCNViewController.h"

//Frameworks
#import <Photos/Photos.h>
#import <AVFoundation/AVFoundation.h>

//Classes
#import "AlertController.h"
#import "SceneViewControl.h"
#import "FocusSquare.h"

// Categories
#import "SCNNode+Cleanup.h"

// Enumerations
#import "RotationAxisEnum.h"

@interface ARSCNViewController ()

// Outlets
@property (weak, nonatomic) IBOutlet ARSCNView* sceneView;

@property (weak, nonatomic) IBOutlet UIButton* addButton;
@property (weak, nonatomic) IBOutlet UIButton* cameraButton;
@property (weak, nonatomic) IBOutlet UIButton* resetButton;

@property (weak, nonatomic) IBOutlet UISegmentedControl* rotationSegmentedControl;
@property (weak, nonatomic) IBOutlet UIBarButtonItem*    rotationControlsBarButton;

@property (weak, nonatomic) IBOutlet UIButton* leftRotationButton;
@property (weak, nonatomic) IBOutlet UIButton* rightRotationButton;
@property (weak, nonatomic) IBOutlet UIButton* cancelRotationButton;

// Properties
@property (strong, nonatomic) AlertController*  alertController;
@property (strong, nonatomic) SceneViewControl* sceneControl;

@property (strong, nonatomic) NSString* objModelTitle;
@property (strong, nonatomic) NSString* currentModelFileFormat;

@property (strong, nonatomic) FocusSquare*    focusSquare;
@property (assign, nonatomic) SceneFileFormat sceneFileFormat;
@property (assign, nonatomic) RotationAxis    rotationControlsAxis;

@property (strong, nonatomic) SCNHitTestResult* removeHitResult;
@property (strong, nonatomic) ARHitTestResult*  initialHitTestResult;

@property (strong, nonatomic) SCNNode* objectModelNode;
@property (strong, nonatomic) SCNNode* movedObject;

@property (assign, nonatomic) CGPoint screenCenter;
@property (assign, nonatomic) CGFloat currentAngleY;
@property (strong, nonatomic) NSURL*  fileModelURL;

@property (assign, nonatomic) SCNVector3 startEulerPosition;

@end

@implementation ARSCNViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self setupScene];
}

- (void) viewWillAppear: (BOOL) animated
{
    [super viewWillAppear: animated];
    
    if ( ARWorldTrackingConfiguration.isSupported )
    {
        [self configureARSession];
        
       self.fileModelURL = [[NSBundle mainBundle] URLForResource: self.objModelTitle
                                                   withExtension: self.currentModelFileFormat];
    }
    else
    {
        [self.alertController showUnsupportedAlert];
    }
}

- (void) viewWillDisappear: (BOOL) animated
{
    [super viewWillDisappear: animated];
    
    [self.sceneView.session pause];
}

- (void) viewWillTransitionToSize: (CGSize)                                    size
        withTransitionCoordinator: (id<UIViewControllerTransitionCoordinator>) coordinator
{
    [super viewWillTransitionToSize: size
          withTransitionCoordinator: coordinator];
    
    CGPoint viewCenter = CGPointMake(size.width  / 2,
                                     size.height / 2);
    
    self.screenCenter = viewCenter;
}

- (void) dealloc
{
    [self.sceneView.scene.rootNode cleanup];
    
    [self.sceneView removeFromSuperview];
    
    [self.sceneView stop: nil];
    
    self.sceneView = nil;
}


#pragma mark - Public methods -

- (void) configureControllerWithTitle: (NSString*)       title
                    withFileExtension: (NSString*)       format
                        withSceneType: (SceneFileFormat) sceneType
{
    self.objModelTitle          = title;
    self.currentModelFileFormat = format;
    self.sceneFileFormat        = sceneType;
}


#pragma mark - Properties -

- (AlertController*) alertController
{
    if ( _alertController == nil )
    {
        _alertController = [[AlertController alloc] initWithOutput: self];
    }
    
    return _alertController;
}

- (SceneViewControl*) sceneControl
{
    if ( _sceneControl == nil )
    {
        _sceneControl = [[SceneViewControl alloc] initWithOutput: self
                                        withAlertControllerInput: self.alertController];
        
        _sceneControl.addObject = YES;
    }
    
    return _sceneControl;
}


#pragma mark - Scene view control output methods -

- (void) changeButtonsState: (BOOL) buttonsState
{
    self.cameraButton.hidden = buttonsState;
    self.addButton.hidden    = buttonsState;
}


#pragma mark - Alert controller ouput methods -

- (void) presentAlertController: (UIViewController*)         viewController
                       animated: (BOOL)                      animated
                     completion: (void (^ __nullable)(void)) completion
{
    [self presentViewController: viewController
                       animated: animated
                     completion: completion];
}


#pragma mark - Actions -

- (IBAction) onAddButtonPressed: (UIButton*) sender
{
    NSArray<ARHitTestResult*>* result = [self.sceneView hitTest: self.screenCenter
                                                          types: ARHitTestResultTypeExistingPlaneUsingExtent];
    
    if ( result.count == 0 )
    {
        return;
    }
    
    ARHitTestResult* hitResult = result.firstObject;
    
    SCNScene* scene = [SCNScene sceneWithURL: self.fileModelURL
                                     options: nil
                                       error: nil];
    
    SCNNode* objectNode;
    
    switch ( self.sceneFileFormat )
    {
        case DaeFileFormat:
        case ScnFileFormat:
        {
            objectNode = [scene.rootNode childNodeWithName: @"fuselage"
                                               recursively: YES];
            
            break;
        }
            
        case ObjFileFormat:
        {
            objectNode = scene.rootNode.childNodes.firstObject;
            
            break;
        }
    }
    
    self.objectModelNode = objectNode;
    
    self.startEulerPosition = self.objectModelNode.eulerAngles;
    
    float insertionYOffset = 0.01;
    
    objectNode.position = SCNVector3Make(hitResult.worldTransform.columns[3].x,
                                         hitResult.worldTransform.columns[3].y - insertionYOffset,
                                         hitResult.worldTransform.columns[3].z);
    
    CGFloat scaleMultiplier = [self obtainOptimalScaleForNode: objectNode];
    
    CGFloat scaleDifferenceFactor = 5;
    
    if ( scaleMultiplier > scaleDifferenceFactor )
    {
        scaleMultiplier = scaleMultiplier * scaleDifferenceFactor;
        
        
        objectNode.scale = SCNVector3Make(objectNode.scale.x / scaleMultiplier,
                                          objectNode.scale.y / scaleMultiplier,
                                          objectNode.scale.z / scaleMultiplier);
    }
    
    [self.sceneView.scene.rootNode addChildNode: objectNode];
    
    [self.focusSquare removeFromParentNode];
    
    self.focusSquare                       = nil;
    self.addButton.hidden                  = YES;
    self.sceneControl.addObject            = NO;
    self.rotationControlsBarButton.enabled = YES;
}

- (IBAction) onResetButtonPressed: (UIButton*) sender
{
    [self refreshSession];
}

- (IBAction) onCameraButtonPressed: (UIButton*) sender
{
    [PHPhotoLibrary requestAuthorization: ^(PHAuthorizationStatus status) {
        
        if ( status == PHAuthorizationStatusAuthorized )
        {
            UIImage* image = [self.sceneView snapshot];
            
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(image: didFinishSavingWithError: contextInfo:), NULL);
        }
        else
        {
            NSString* accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"NSPhotoLibraryUsageDescription"];
            
            [self.alertController showPermissionAlertWithDescription: accessDescription];
        }
    }];
}

- (IBAction) onRotationSegmentedControlPressed: (UISegmentedControl*) sender
{
    self.rotationControlsAxis = sender.selectedSegmentIndex;
}

- (IBAction) onLeftRotationButtonPressed: (UIButton*) sender
{
    [self startRotationWithForwardDirectionFlag: NO
                              withRepeatForever: NO];
}

- (IBAction) onRightRotationButtonPressed: (UIButton*) sender
{
    [self startRotationWithForwardDirectionFlag: YES
                              withRepeatForever: NO];
}

- (IBAction) onCancelRotationButtonPressed: (UIButton*) sender
{
    sender.hidden = YES;
    
    self.objectModelNode.eulerAngles = self.startEulerPosition;
}

- (IBAction) onRotationControlsBarButtonPressed: (UIBarButtonItem*) sender
{
    if ( self.rotationControlsBarButton.tag == 0 )
    {
        self.rotationControlsBarButton.tag = 1;
        
        self.rotationControlsBarButton.tintColor = [UIColor yellowColor];
        
        self.rotationSegmentedControl.hidden = NO;
        self.leftRotationButton.hidden       = NO;
        self.rightRotationButton.hidden      = NO;
        
        if ( self.objectModelNode.eulerAngles.x != self.startEulerPosition.x ||
             self.objectModelNode.eulerAngles.y != self.startEulerPosition.y ||
             self.objectModelNode.eulerAngles.z != self.startEulerPosition.z )
        {
            self.cancelRotationButton.hidden = NO;
        }
    }
    else
    {
        self.rotationControlsBarButton.tag = 0;
        
        self.rotationControlsBarButton.tintColor = [UIColor whiteColor];
        
        self.rotationSegmentedControl.hidden = YES;
        self.leftRotationButton.hidden       = YES;
        self.rightRotationButton.hidden      = YES;
        self.cancelRotationButton.hidden     = YES;
    }
}


#pragma mark - Configure ARSession -

- (void) refreshSession
{
    [self.sceneView.scene.rootNode cleanup];
    
    self.focusSquare                       = nil;
    self.addButton.hidden                  = NO;
    self.sceneControl.addObject            = YES;
    self.rotationControlsBarButton.enabled = NO;
    self.rotationControlsBarButton.tag     = 1;
    
    [self onRotationControlsBarButtonPressed: self.rotationControlsBarButton];
    
    [self configureARSession];
}

- (void) configureARSession
{
    ARWorldTrackingConfiguration* configuration = [ARWorldTrackingConfiguration new];
    
    configuration.planeDetection = ARPlaneDetectionHorizontal;
    
    [self.sceneView.session runWithConfiguration: configuration
                                         options: ARSessionRunOptionResetTracking |
                                                  ARSessionRunOptionRemoveExistingAnchors];
    
    [self checkMediaPermissionAndButtonState];
}

- (void) setupScene
{
    self.sceneView.delegate                     = self.sceneControl;
    self.sceneView.showsStatistics              = NO;
    self.sceneView.debugOptions                 = SCNDebugOptionNone;
    self.sceneView.autoenablesDefaultLighting   = YES;
    self.sceneView.automaticallyUpdatesLighting = YES;
    self.sceneView.scene                        = [SCNScene new];
    self.screenCenter                           = self.view.center;
    self.rotationControlsAxis                   = XRotationAxis;
}


#pragma mark - SceneViewControlOutput methods -

- (FocusSquare*) obtainFocusSquare
{
    return self.focusSquare;
}

- (void) addNodeToSceneWithFocusNode: (FocusSquare*) focusNode
{
    [self.sceneView.scene.rootNode addChildNode: focusNode];
    
    self.focusSquare = focusNode;
}

- (void) configureFocusSquare
{
    NSArray<ARHitTestResult*>* hitTest = [self.sceneView hitTest: self.screenCenter
                                                           types: ARHitTestResultTypeExistingPlane];
    
    ARHitTestResult* hitTestResult = hitTest.firstObject;
    
    self.focusSquare.position = SCNVector3Make(hitTestResult.worldTransform.columns[3].x,
                                               hitTestResult.worldTransform.columns[3].y,
                                               hitTestResult.worldTransform.columns[3].z);
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        [self updateFocusSquare];
    });
}

- (void) updateFocusSquare
{
    NSArray<ARHitTestResult*>* hitTest = [self.sceneView hitTest: self.screenCenter
                                                           types: ARHitTestResultTypeExistingPlaneUsingExtent];
    
    ARHitTestResult* hitTestResult = hitTest.firstObject;
    
    BOOL canAddNewModel = [hitTestResult.anchor isKindOfClass: [ARPlaneAnchor class]];
    
    if (canAddNewModel)
    {
        [self.focusSquare configureFocusSquare: canAddNewModel];
        
        self.addButton.tintColor = UIColor.whiteColor;
    }
    else
    {
        [self.focusSquare configureFocusSquare: NO];
        
        self.addButton.tintColor = UIColor.clearColor;
    }
}


#pragma mark - Media Premission Check -

- (void) checkMediaPermissionAndButtonState
{
    dispatch_async(dispatch_get_main_queue(), ^{
       
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType: AVMediaTypeVideo];
        
        if ( status == AVAuthorizationStatusAuthorized ||
             status == AVAuthorizationStatusNotDetermined )
        {
            [self.alertController showOverlyText: @"STARTING A NEW SESSION, TRY MOVING LEFT OR RIGHT"
                                    withDuration: 2];
        }
        else
        {
            NSString* accessDescription = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"NSCameraUsageDescription"];
            
            [self.alertController showPermissionAlertWithDescription: accessDescription];
        }
        
        self.cameraButton.hidden = YES;
        self.addButton.hidden    = YES;
    });
}


#pragma mark - Write Image CompletionHandler -

- (void)           image: (UIImage*) image
didFinishSavingWithError: (NSError*) error
             contextInfo: (void*) contextInfo
{
    if ( error )
    {
        [self.alertController showOverlyText: @"Error saving snapshot."
                                withDuration: 2];
    }
    else
    {
        [self.alertController showOverlyText: @"Saved snapshot successfully."
                                withDuration: 2];
    }
}


#pragma mark - GestureActions methods -

- (IBAction) onScaleObjectGesture: (UIPinchGestureRecognizer*) sender
{
    if ( sender.state == UIGestureRecognizerStateBegan )
    {
        CGPoint tapPoint = [sender locationOfTouch: 1
                                            inView: self.sceneView];
        
        NSArray<SCNHitTestResult*>* result = [self.sceneView hitTest: tapPoint
                                                             options: nil];
        if ( result.count == 0 )
        {
            tapPoint = [sender locationOfTouch: 0
                                        inView: self.sceneView];
            
            result = [self.sceneView hitTest: tapPoint
                                     options: nil];
            
            if ( result.count == 0 )
                return;
        }
    
        self.movedObject = self.objectModelNode;
    }
    
    if ( sender.state == UIGestureRecognizerStateChanged )
    {
        if ( self.movedObject )
        {
            CGFloat pinchScaleX = sender.scale * self.movedObject.scale.x;
            CGFloat pinchScaleY = sender.scale * self.movedObject.scale.y;
            CGFloat pinchScaleZ = sender.scale * self.movedObject.scale.z;
            
            [self.movedObject setScale: SCNVector3Make(pinchScaleX, pinchScaleY, pinchScaleZ)];
        }
        
        sender.scale = 1;
    }
    
    if ( sender.state == UIGestureRecognizerStateEnded )
    {
        self.movedObject = nil;
    }
}

- (IBAction) onRotateObjectGesture: (UIRotationGestureRecognizer*) sender
{
    CGFloat rotation = sender.rotation;
    
    CGPoint tapPoint = [sender locationInView: self.sceneView];
    
    NSArray<SCNHitTestResult*>* result = [self.sceneView hitTest: tapPoint
                                                         options: nil];
    
    if ( result.count == 0 )
        return;
    
    SCNNode* rotatedNode = self.objectModelNode;
    
    if ( sender.state == UIGestureRecognizerStateChanged )
    {
        rotatedNode.eulerAngles = SCNVector3Make(rotatedNode.eulerAngles.x,
                                                 self.currentAngleY - rotation,
                                                 rotatedNode.eulerAngles.z);
    }
    
    if ( sender.state == UIGestureRecognizerStateEnded )
    {
        self.currentAngleY = rotatedNode.eulerAngles.y;
    }
}

- (IBAction) onMoveObjectGesture: (UIPanGestureRecognizer*) sender
{
    if ( sender.state == UIGestureRecognizerStateBegan )
    {
        CGPoint tapPoint = [sender locationInView: self.sceneView];
        
        NSArray<SCNHitTestResult*>* result = [self.sceneView hitTest: tapPoint
                                                             options: nil];
        
        NSArray<ARHitTestResult*>* hitResults = [self.sceneView hitTest: tapPoint
                                                                  types: ARHitTestResultTypeFeaturePoint];
        if ( result.count == 0 )
            return;
        
        self.movedObject = self.objectModelNode;
        
        self.initialHitTestResult = hitResults.firstObject;
    }
    else if ( sender.state == UIGestureRecognizerStateChanged )
    {
        if ( self.movedObject )
        {
            CGPoint tapPoint = [sender locationInView: self.sceneView];
            
            NSArray<ARHitTestResult*>* hitResults = [self.sceneView hitTest: tapPoint
                                                                      types: ARHitTestResultTypeFeaturePoint];
            ARHitTestResult* result = hitResults.firstObject;
            
            [SCNTransaction begin];
            
            SCNMatrix4 initialMatrix = SCNMatrix4FromMat4(self.initialHitTestResult.worldTransform);
            
            SCNVector3 initialVector = SCNVector3Make(initialMatrix.m41,
                                                      initialMatrix.m42,
                                                      initialMatrix.m43);
            
            SCNMatrix4 matrix = SCNMatrix4FromMat4(result.worldTransform);
            
            SCNVector3 vector = SCNVector3Make(matrix.m41,
                                               matrix.m42,
                                               matrix.m43);
            
            CGFloat dx = vector.x - initialVector.x;
            CGFloat dz = vector.z - initialVector.z;
            
            SCNVector3 newPositionVector = SCNVector3Make(self.movedObject.position.x + dx,
                                                          self.movedObject.position.y,
                                                          self.movedObject.position.z + dz);
            
            [self.movedObject setPosition: newPositionVector];
            
            [SCNTransaction commit];
            
            self.initialHitTestResult = result;
        }
    }
    else
        if ( sender.state == UIGestureRecognizerStateEnded ) 
        {
            self.movedObject          = nil;
            self.initialHitTestResult = nil;
        }
}

- (IBAction) onRightRotationGesture: (UILongPressGestureRecognizer*) sender
{
    if ( sender.state == UIGestureRecognizerStateBegan )
    {
        [self startRotationWithForwardDirectionFlag: YES
                                  withRepeatForever: YES];
    }
    
    if ( sender.state == UIGestureRecognizerStateEnded )
    {
        [self.objectModelNode removeAllActions];
    }
}

- (IBAction) onLeftRotationGesture: (UILongPressGestureRecognizer*) sender
{
    if ( sender.state == UIGestureRecognizerStateBegan )
    {
        [self startRotationWithForwardDirectionFlag: NO
                                  withRepeatForever: YES];
    }
    
    if ( sender.state == UIGestureRecognizerStateEnded )
    {
        [self.objectModelNode removeAllActions];
    }
}


#pragma mark - Internal methods

- (void) startRotationWithForwardDirectionFlag: (BOOL) forwardDirection
                             withRepeatForever: (BOOL) repeatForever
{
    CGFloat xRotation = 0;
    CGFloat yRotation = 0;
    CGFloat zRotation = 0;
    
    CGFloat directionFactor = 1;
    
    if ( forwardDirection == NO )
        directionFactor = -1;
    
    CGFloat rotationValue =  0.3 * directionFactor;
    
    switch ( self.rotationControlsAxis )
    {
        case XRotationAxis:
            xRotation = rotationValue;
            break;
        case YRotationAxis:
            yRotation = rotationValue;
            break;
        case ZRotationAxis:
            zRotation = rotationValue;
            break;
    }
    
    SCNAction* rotateAction = [SCNAction rotateByX: xRotation
                                                 y: yRotation
                                                 z: zRotation
                                          duration: 0.5];
    
    if ( repeatForever == YES )
    {
        rotateAction.duration = 0.2;
        
        [self.objectModelNode runAction: [SCNAction repeatActionForever: rotateAction]];
    }
    else
    {
        [self.objectModelNode runAction: rotateAction];
    }
    
    self.cancelRotationButton.hidden = NO;
}

- (CGFloat) obtainOptimalScaleForNode: (SCNNode*) node
{
    CGFloat normalizeWidth  = 0.297;
    CGFloat normalizeHeight = 0.082;
    CGFloat normalizeDepth  = 0.302;
    
    NSNumber* widthMultiplier  = [NSNumber numberWithFloat: (node.scale.x / normalizeWidth)];
    NSNumber* heightMultiplier = [NSNumber numberWithFloat: (node.scale.y / normalizeHeight)];
    NSNumber* depthMultiplier  = [NSNumber numberWithFloat: (node.scale.z / normalizeDepth)];
    
    NSArray* scaleMultipliers = @[widthMultiplier, heightMultiplier, depthMultiplier];
    
    NSNumber* maxScaleMultiplier = [scaleMultipliers valueForKeyPath: @"@max.doubleValue"];
    
    return maxScaleMultiplier.doubleValue;
}

@end
