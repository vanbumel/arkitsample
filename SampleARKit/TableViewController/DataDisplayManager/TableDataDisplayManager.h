//
//  TableDataDisplayManager.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/9/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// Protocols
#import "TableDataDisplayManagerInput.h"
#import "TableDataDisplayManagerOutput.h"

// Enumerations
#import "SceneFileFormatEnum.h"

NS_ASSUME_NONNULL_BEGIN

@interface TableDataDisplayManager : NSObject <UITableViewDataSource, UITableViewDelegate>

- (instancetype) initWithDataSource: (id<TableDataDisplayManagerInput>)  dataSource
                         withOutput: (id<TableDataDisplayManagerOutput>) output;

/**
 @author Vladyslav Bedro
 
 Method for setting sceneFileFormat in model
 
 @param sceneFileFormat - format of scene file
 */
- (void) configureDDMWithSFileFormat: (SceneFileFormat) sceneFileFormat;

@end

NS_ASSUME_NONNULL_END
