//
//  ARSCNViewController.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/7/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

//Frameworks
#import <ARKit/ARKit.h>
#import <SceneKit/SceneKit.h>

//Protocols
#import "SceneViewControlOutput.h"
#import "AlertControllerOutput.h"

// Enumerations
#import "SceneFileFormatEnum.h"

@interface ARSCNViewController : UIViewController <SceneViewControlOutput, AlertControllerOutput>

- (void) configureControllerWithTitle: (NSString*)       title
                    withFileExtension: (NSString*)       format
                        withSceneType: (SceneFileFormat) sceneType;

@end

