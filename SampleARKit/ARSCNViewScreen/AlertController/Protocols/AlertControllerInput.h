//
//  AlertControllerInput.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/11/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AlertControllerInput <NSObject>

- (void) showOverlyText: (NSString*) text
           withDuration: (int)       duration;

- (void) showUnsupportedAlert;

- (void) showPermissionAlertWithDescription: (NSString*) accessDescription;

@end

NS_ASSUME_NONNULL_END
