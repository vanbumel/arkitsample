//
//  SCNNode+Cleanup.m
//  SceneKitCheatSheets
//
//  Created by Vladyslav Bedro on 4/30/19.
//  Copyright © 2019 Vladyslav Bedro. All rights reserved.
//

#import "SCNNode+Cleanup.h"

// Frameworks
#import <SceneKit/SceneKit.h>

@implementation SCNNode (Cleanup)


#pragma mark - Public methods -

- (void) cleanup
{
    for ( SCNNode* childNode in self.childNodes )
    {
        [childNode cleanup];
        
        [childNode removeFromParentNode];
    }
    
    for ( int i = 0; i < self.geometry.materials.count; i++ )
    {
        self.geometry.materials[i].diffuse.contents          = nil;
        self.geometry.materials[i].normal.contents           = nil;
        self.geometry.materials[i].specular.contents         = nil;
        self.geometry.materials[i].reflective.contents       = nil;
        self.geometry.materials[i].ambient.contents          = nil;
        self.geometry.materials[i].emission.contents         = nil;
        self.geometry.materials[i].transparent.contents      = nil;
        self.geometry.materials[i].multiply.contents         = nil;
        self.geometry.materials[i].selfIllumination.contents = nil;
        self.geometry.materials[i].ambientOcclusion.contents = nil;
        self.geometry.materials[i].displacement.contents     = nil;
        self.geometry.materials[i].metalness.contents        = nil;
        self.geometry.materials[i].roughness.contents        = nil;
        self.geometry.materials[i].metalness.contents        = nil;
        self.geometry.materials[i].roughness.contents        = nil;
        
        SCNMaterial* material = self.geometry.materials[i];
        
        material = nil;
        
        [self.geometry removeMaterialAtIndex: i];
    }
    
    self.geometry = nil;
}

@end
