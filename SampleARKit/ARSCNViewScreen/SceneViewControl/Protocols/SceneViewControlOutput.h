//
//  SceneViewControlOutput.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/11/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

// Classes
#import "FocusSquare.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SceneViewControlOutput <NSObject>

- (void) changeButtonsState: (BOOL) buttonsState;

/**
 @author Ivan Chaban
 
 Method for refresh session, and start new session
 */
- (void) refreshSession;

/**
 @author Vladyslav Bedro
 
 Method obtains focusSquare object from view controller
 
 @return focusSquare - focus object for view
 */
- (FocusSquare*) obtainFocusSquare;

/**
 @author Vladyslav Bedro
 
 Method add node as child node to root node of scene on sceneView
 
 @param focusNode - added node on scene
 */
- (void) addNodeToSceneWithFocusNode: (FocusSquare*) focusNode;

/**
 @author Vladyslav Bedro
 
 Method configure correct position for focus square on scene center
 */
- (void) configureFocusSquare;

@end

NS_ASSUME_NONNULL_END
