//
//  TableModel.m
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/9/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "TableModel.h"

@interface TableModel()

// Properties
@property (strong, nonatomic) NSArray* daeSceneModelsArray;
@property (strong, nonatomic) NSArray* scnSceneModelsArray;
@property (strong, nonatomic) NSArray* objSceneModelsArray;

@end

@implementation TableModel


#pragma mark - Properties -

- (NSArray*) daeSceneModelsArray
{
    if ( _daeSceneModelsArray == nil )
    {
        _daeSceneModelsArray = @[@"piper_pa18",
                                 @"Jet_Fighter",
                                 @"Boeing_787_8_1",
                                 @"SC_Private_001",
                                 @"Airplane24810",
                                 @"Kfir_TC10"];
    }
    
    return _daeSceneModelsArray;
}

- (NSArray*) scnSceneModelsArray
{
    if ( _scnSceneModelsArray == nil )
    {
        _scnSceneModelsArray = @[@"piper_pa18_SCN",
                                 @"Jet_Fighter_SCN",
                                 @"Boeing_787_8_1_SCN",
                                 @"SC_Private_001_SCN",
                                 @"Airplane24810_SCN",
                                 @"Kfir_TC10_SCN"];
    }
    
    return _scnSceneModelsArray;
}

- (NSArray*) objSceneModelsArray
{
    if ( _objSceneModelsArray == nil )
    {
        _objSceneModelsArray = @[@"piper_pa18",
                                 @"T_50",
                                 //@"11803_Airplane_v1_l1", // oversize
                                 @"airplane_engine",
//                                 @"Seahawk", // oversize
//                                 @"11805_airplane_v2_L2", //  oversize
                                 @"biplane_complete",
//                                 @"11804_Airplane_v2_l2", //oversize
                                 @"DorandAR1",
//                                 @"10621_CoastGuardHelicopter", // oversize
//                                 @"passenger_transport_landed", // oversize
                                 @"10593_Fighter_Jet_SG_v1_iterations-2",
//                                 @"Sirus_Transport.landed", // oversize
                                 @"TAL16OBJ"];
    }
    
    return _objSceneModelsArray;
}


#pragma mark - TableDataDisplayManagerInput protocol methods -

- (NSArray*) fetchTableContentForDaeFormat
{
    return self.daeSceneModelsArray;
}

- (NSArray*) fetchTableContentForScnFormat
{
    return self.scnSceneModelsArray;
}

- (NSArray*) fetchTableContentForObjFormat
{
    return self.objSceneModelsArray;
}

@end
