//
//  MainScreenCell.h
//  SceneKitCheatSheets
//
//  Created by Vladyslav Bedro on 4/17/19.
//  Copyright © 2019 Vladyslav Bedro. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainScreenCell : UITableViewCell

/**
 @author Vladyslav Bedro
 
 Method for filling cell with title
 
 @param title - title of sceneKit object model
 */
- (void) fillContentWithTitle: (NSString*) title;

@end

NS_ASSUME_NONNULL_END
