//
//  MainScreenCell.m
//  SceneKitCheatSheets
//
//  Created by Vladyslav Bedro on 4/17/19.
//  Copyright © 2019 Vladyslav Bedro. All rights reserved.
//

#import "MainScreenCell.h"

@interface MainScreenCell()

// Outlets
@property (weak, nonatomic) IBOutlet UILabel* titleLabel;

@end

@implementation MainScreenCell


#pragma mark - Life cycle -

- (void) awakeFromNib
{
    [super awakeFromNib];
}

- (void) setSelected: (BOOL) selected
            animated: (BOOL) animated
{
    [super setSelected: selected
              animated: animated];
}


#pragma mark - Public methods -

- (void) fillContentWithTitle: (NSString*) title
{
    self.titleLabel.text = title;
}

@end
