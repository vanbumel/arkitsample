//
//  FocusSquare.h
//  SampleARKit
//
//  Created by OnSight on 5/13/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

// Frameworks
#import <SceneKit/SceneKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FocusSquare : SCNNode

- (void) configureFocusSquare: (BOOL) isClosed;

@end

NS_ASSUME_NONNULL_END
