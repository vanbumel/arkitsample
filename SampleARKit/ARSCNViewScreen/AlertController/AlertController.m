//
//  AlertController.m
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/7/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "AlertController.h"


@interface AlertController()

// properties
@property (weak, nonatomic) id<AlertControllerOutput> output;

@end

@implementation AlertController


#pragma mark - Initialization -

- (instancetype) initWithOutput: (id<AlertControllerOutput>) output
{
    if ( self = [super init] )
    {
        self.output = output;
    }
    
    return self;
}


- (void) showUnsupportedAlert
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle: @"Unsupported platform"
                                                                   message: @"This app requires world tracking. World tracking is only available on iOS 11 with A9 processor devices or newer."
                                                            preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction* noAction = [UIAlertAction actionWithTitle: @"OK"
                                                       style: UIAlertActionStyleDefault
                                                     handler: ^(UIAlertAction* action) {
                                                     }];
    [alert addAction: noAction];
    
    [self.output presentAlertController: alert
                               animated: YES
                             completion: nil];
}

- (void) showOverlyText: (NSString*) text
           withDuration: (int)       duration
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle: nil
                                                                   message: text
                                                            preferredStyle: UIAlertControllerStyleAlert];
    
    [self.output presentAlertController: alert
                               animated: YES
                             completion: nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration* NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated: YES
                                  completion: nil];
    });
}

- (void) showPermissionAlertWithDescription: (NSString*) accessDescription
{
    UIAlertController* alertController = [UIAlertController alertControllerWithTitle: accessDescription
                                                                             message: @"To give permission tap on 'Change Settings' button."
                                                                      preferredStyle: UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle: @"Cancel"
                                                           style: UIAlertActionStyleCancel
                                                         handler: nil];
    [alertController addAction: cancelAction];
    
    UIAlertAction* settingsAction = [UIAlertAction actionWithTitle: @"Change Settings"
                                                             style: UIAlertActionStyleDefault
                                                           handler: ^(UIAlertAction* _Nonnull action) {

        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString]
                                           options: @{}
                                 completionHandler: nil];
    }];
    
    [alertController addAction: settingsAction];
    
    [self.output presentAlertController: alertController
                               animated: YES
                             completion: nil];
}

@end
