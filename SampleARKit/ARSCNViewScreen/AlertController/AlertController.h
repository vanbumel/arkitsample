//
//  AlertController.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/7/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//Protocol
#import "AlertControllerOutput.h"
#import "AlertControllerInput.h"

NS_ASSUME_NONNULL_BEGIN

@interface AlertController : NSObject <AlertControllerInput>

- (instancetype) initWithOutput: (id<AlertControllerOutput>) output;

@end

NS_ASSUME_NONNULL_END
