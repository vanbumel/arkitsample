//
//  ViewController.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/7/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

//Frameworks
#import <ARKit/ARKit.h>

//Protocols
#import "SceneViewControlOutput.h"
#import "AlertControllerOutput.h"

@interface ARSCNViewController : UIViewController <SceneViewControlOutput, AlertControllerOutput>

- (void) configureControllerWith: (NSString*) objName;

@end

