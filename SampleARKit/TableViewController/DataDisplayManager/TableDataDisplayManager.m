//
//  TableDataDisplayManager.m
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/9/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "TableDataDisplayManager.h"

// Classes
#import "MainScreenCell.h"

// Constants
#import "MainScreenConstants.h"

@interface TableDataDisplayManager()

// properties
@property (weak, nonatomic) id<TableDataDisplayManagerOutput> output;
@property (weak, nonatomic) id<TableDataDisplayManagerInput> dataSource;

@property (strong, nonatomic) NSArray* contentArray;

@end

@implementation TableDataDisplayManager


#pragma mark - Initialization -

- (instancetype) initWithDataSource: (id<TableDataDisplayManagerInput>)  dataSource
                         withOutput: (id<TableDataDisplayManagerOutput>) output
{
    if ( self = [super init] )
    {
        self.dataSource = dataSource;
        self.output     = output;
    }
    
    return self;
}


#pragma mark - Properties -

- (NSArray*) contentArray
{
    if (_contentArray == nil)
    {
        _contentArray = [self.dataSource fetchTableContentForDaeFormat];
    }
    
    return _contentArray;
}


#pragma mark - TableViewDataSoure methods -

- (NSInteger) tableView: (UITableView*) tableView
  numberOfRowsInSection: (NSInteger)    section
{
    return self.contentArray.count;
}

- (UITableViewCell*) tableView: (UITableView*) tableView
         cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    MainScreenCell* cell = [tableView dequeueReusableCellWithIdentifier: kMainScreenCellIdentifier
                                                           forIndexPath: indexPath];
    
    [cell fillContentWithTitle: self.contentArray[indexPath.row]];
    
    return cell;
}


#pragma mark - TableViewDelegate methods -

- (void)      tableView: (UITableView*) tableView
didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
    [tableView deselectRowAtIndexPath: indexPath
                             animated: YES];
    
    [self.output didSelectRowWith: self.contentArray[indexPath.row]];
}


#pragma mark - Public methods -

- (void) configureDDMWithSFileFormat: (SceneFileFormat) sceneFileFormat
{
    switch ( sceneFileFormat )
    {
        case DaeFileFormat:
        {
            self.contentArray = [self.dataSource fetchTableContentForDaeFormat];
            
            break;
        }
            
        case ScnFileFormat:
        {
            self.contentArray = [self.dataSource fetchTableContentForScnFormat];
            
            break;
        }
            
        case ObjFileFormat:
        {
            self.contentArray = [self.dataSource fetchTableContentForObjFormat];
            
            break;
        }
    }
}

@end
