//
//  TableDataDisplayManagerInput.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/9/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TableDataDisplayManagerInput <NSObject>

/**
 @author Vladyslav Bedro
 
 Method for fetching table content from main screen
 
 @return array with sceneKit title models for .dae files
 */
- (NSArray*) fetchTableContentForDaeFormat;

/**
 @author Vladyslav Bedro
 
 Method for fetching table content from main screen
 
 @return array with sceneKit title models for .scn files
 */
- (NSArray*) fetchTableContentForScnFormat;

/**
 @author Vladyslav Bedro
 
 Method for fetching table content from main screen
 
 @return array with sceneKit title models for .obj files
 */
- (NSArray*) fetchTableContentForObjFormat;

@end

NS_ASSUME_NONNULL_END
