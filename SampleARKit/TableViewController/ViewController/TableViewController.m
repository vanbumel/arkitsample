//
//  TableViewController.m
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/9/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "TableViewController.h"

#import "TableModel.h"
#import "TableDataDisplayManager.h"
#import "ARSCNViewController.h"

// Constants
#import "MainScreenConstants.h"

// Enumerations
#import "SceneFileFormatEnum.h"

@interface TableViewController()

// properties
@property (weak, nonatomic) IBOutlet UITableView* tableView;

@property (strong, nonatomic) TableDataDisplayManager* displayManager;
@property (strong, nonatomic) TableModel*              model;

@property (strong, nonatomic) NSString* selectedObjModel;
@property (strong, nonatomic) NSString* sceneFileFormat;

@property (assign, nonatomic) SceneFileFormat sceneType;

@end

@implementation TableViewController


#pragma mark - Life cycle -

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self bindingUI];
}


#pragma mark - Properties -

- (TableDataDisplayManager*) displayManager
{
    if ( _displayManager == nil )
    {
        _displayManager = [[TableDataDisplayManager alloc] initWithDataSource: self.model
                                                                   withOutput: self];
    }
    
    return _displayManager;
}

- (TableModel*) model
{
    if ( _model == nil )
    {
        _model = [TableModel new];
    }
    
    return _model;
}


#pragma mark - TableDataDisplayManagerOutput methods -

- (void) didSelectRowWith: (NSString*) objName
{
    self.selectedObjModel = objName;
    
    [self performSegueWithIdentifier: @"ShowARSceneViewController"
                              sender: self];
}


#pragma mark - Segue -

- (void) prepareForSegue: (UIStoryboardSegue*) segue
                  sender: (id) sender
{
    if ( [segue.identifier isEqualToString: @"ShowARSceneViewController"] )
    {
        ARSCNViewController* controller = segue.destinationViewController;
        
        [controller configureControllerWithTitle: self.selectedObjModel
                               withFileExtension: self.sceneFileFormat
                                   withSceneType: self.sceneType];
    }
}


#pragma mark - Internal methods -

- (void) bindingUI
{
    self.tableView.dataSource = self.displayManager;
    self.tableView.delegate   = self.displayManager;
    
    self.tableView.editing                      = NO;
    self.tableView.allowsSelectionDuringEditing = NO;
    
    self.sceneFileFormat = @"dae";
    
    [self.tableView registerNib: [UINib nibWithNibName: kMainScreenCell
                                                bundle: nil]
         forCellReuseIdentifier: kMainScreenCellIdentifier];
}


#pragma mark - Actions -

- (IBAction) onFileFormatSControlPressed: (UISegmentedControl*) sender
{
    switch ( sender.selectedSegmentIndex )
    {
        case DaeFileFormat:
        {
            [self.displayManager configureDDMWithSFileFormat: DaeFileFormat];
            
            self.sceneFileFormat = @"dae";
            
            self.sceneType = DaeFileFormat;
            
            break;
        }
            
        case ScnFileFormat:
        {
            [self.displayManager configureDDMWithSFileFormat: ScnFileFormat];
            
            self.sceneFileFormat = @"scn";
            
            self.sceneType = ScnFileFormat;
            
            break;
        }
            
        case ObjFileFormat:
        {
            [self.displayManager configureDDMWithSFileFormat: ObjFileFormat];
            
            self.sceneFileFormat = @"obj";
            
            self.sceneType = ObjFileFormat;
            
            break;
        }
    }
    
    [self.tableView reloadData];
}

@end
