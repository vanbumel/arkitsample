//
//  SceneViewControl.m
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/7/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "SceneViewControl.h"

@interface SceneViewControl()

// properties
@property (weak, nonatomic) id<SceneViewControlOutput> output;
@property (weak, nonatomic) id<AlertControllerInput> alertController;

@end

@implementation SceneViewControl


#pragma mark - Initialization -

- (instancetype) initWithOutput: (id<SceneViewControlOutput>) output
       withAlertControllerInput: (id<AlertControllerInput>)   alertController
{
    if ( self = [super init] )
    {
        self.output          = output;
        self.alertController = alertController;
    }
    
    return self;
}


#pragma mark - ARSCNViewDelegate -

- (void) renderer: (id<SCNSceneRenderer>) renderer
       didAddNode: (SCNNode*)             node
        forAnchor: (ARAnchor*)            anchor
{
    if ( ![anchor isKindOfClass: [ARPlaneAnchor class]] )
        return;
    
    if ( self.addObject == YES )
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.output changeButtonsState: NO];
            
            [self.alertController showOverlyText: @"SURFACE DETECTED, TAP TO ON ADD OBJECT"
                                    withDuration: 2];
        });
        
        if ( [self.output obtainFocusSquare] == nil )
        {
            FocusSquare* focusSquareLocal = [FocusSquare new];
            
            [self.output addNodeToSceneWithFocusNode: focusSquareLocal];
        }
    }
}

- (void) renderer: (id<SCNSceneRenderer>) renderer
    didUpdateNode: (SCNNode*)             node
        forAnchor: (ARAnchor*)            anchor
{
    if ( ![anchor isKindOfClass: [ARPlaneAnchor class]] )
        return;
}

- (void) renderer: (id<SCNSceneRenderer>) renderer
    didRemoveNode: (SCNNode*)             node
        forAnchor: (ARAnchor*)            anchor
{
    if ( ![anchor isKindOfClass: [ARPlaneAnchor class]] )
        return;
}

- (void) renderer: (id<SCNSceneRenderer>) renderer
     updateAtTime: (NSTimeInterval)       time
{
    [self.output configureFocusSquare];
}


#pragma mark - ARSessionObserver -

- (void) session: (ARSession*) session
didFailWithError: (NSError*)   error
{
    [self.alertController showOverlyText: @"PLEASE TRY RESETTING THE SESSION"
                            withDuration: 3];
}

- (void) sessionWasInterrupted: (ARSession*) session
{
    [self.alertController showOverlyText: @"SESSION INTERRUPTED"
                            withDuration: 3];
}

- (void) sessionInterruptionEnded: (ARSession*) session
{
    [self.output refreshSession];
}

@end
