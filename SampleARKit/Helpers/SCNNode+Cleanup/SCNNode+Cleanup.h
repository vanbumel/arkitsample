//
//  SCNNode+Cleanup.h
//  SceneKitCheatSheets
//
//  Created by Vladyslav Bedro on 4/30/19.
//  Copyright © 2019 Vladyslav Bedro. All rights reserved.
//

#import <SceneKit/SceneKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SCNNode (Cleanup)

/**
 @author Vladyslav Bedro
 
 Method cleans memory for calling node
 */
- (void) cleanup;

@end

NS_ASSUME_NONNULL_END
