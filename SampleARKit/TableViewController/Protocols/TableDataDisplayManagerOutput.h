//
//  TableDataDisplayManagerOutput.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/9/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TableDataDisplayManagerOutput <NSObject>

- (void) didSelectRowWith: (NSString*) objName;

@end

NS_ASSUME_NONNULL_END
