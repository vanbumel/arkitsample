//
//  RotationAxisEnum.h
//  SampleARKit
//
//  Created by OnSight on 5/15/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#ifndef RotationAxisEnum_h
#define RotationAxisEnum_h

typedef NS_ENUM(NSUInteger, RotationAxis)
{
    XRotationAxis,
    YRotationAxis,
    ZRotationAxis
};

#endif /* RotationAxisEnum_h */
