//
//  SceneViewControl.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/7/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>

//Frameworks
#import <ARKit/ARKit.h>

//Protocols
#import "SceneViewControlOutput.h"
#import "AlertControllerInput.h"

NS_ASSUME_NONNULL_BEGIN

@interface SceneViewControl : NSObject <ARSCNViewDelegate, ARSessionDelegate>

@property (assign, nonatomic) BOOL addObject;

- (instancetype) initWithOutput: (id<SceneViewControlOutput>) output
       withAlertControllerInput: (id<AlertControllerInput>)   alertController;

@end

NS_ASSUME_NONNULL_END
