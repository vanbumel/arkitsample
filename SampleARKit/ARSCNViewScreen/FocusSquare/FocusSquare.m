//
//  FocusSquare.m
//  SampleARKit
//
//  Created by OnSight on 5/13/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import "FocusSquare.h"

// Frameworks
#import <GLKit/GLKit.h>

@implementation FocusSquare


#pragma mark - Initialization -

- (instancetype) init
{
    if ( self = [super init] )
    {
        SCNPlane* plane = [SCNPlane planeWithWidth: 0.1
                                            height: 0.1];
        
        plane.firstMaterial.diffuse.contents = [UIImage imageNamed: @"closeIcon"];
        plane.firstMaterial.doubleSided      = YES;
        
        self.geometry = plane;
        
        self.eulerAngles = SCNVector3Make(GLKMathDegreesToRadians(-90),
                                          self.eulerAngles.y,
                                          self.eulerAngles.z);
    }
    
    return self;
}

- (void) configureFocusSquare: (BOOL) isClosed
{
    UIImage* image = (isClosed) ? [UIImage imageNamed: @"closeIcon"] : [UIImage imageNamed: @"openIcon"];
    
    self.geometry.firstMaterial.diffuse.contents = image;
}

@end
