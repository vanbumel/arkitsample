//
//  AlertControllerOutput.h
//  SampleARKit
//
//  Created by OnSight MacBook Pro on 5/7/19.
//  Copyright © 2019 OnSight MacBook Pro. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol AlertControllerOutput <NSObject>

- (void) presentAlertController: (UIViewController*)         viewController
                       animated: (BOOL)                      animated
                     completion: (void (^ __nullable)(void)) completion;

@end

NS_ASSUME_NONNULL_END
